const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");

const app = express();
app.set('view engine', "ejs")
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded ({extended:true}));


let datos = [{
    matricula:"2020030324",
    nombre: "Víctor Uriel Juárez Lugo",
    sexo: "H",
    materias:["Inglés", " Tecnologías de internet", " Base de datos"]
},
{
    matricula:"2020030325",
    nombre: "Víctoria Juárez López",
    sexo: "M",
    materias:["Español", " Matemáticas", " Base de datos"]

},
{
    matricula:"2020030312",
    nombre: "Lizbet Argelia Padilla Moreno",
    sexo: "M",
    materias:["Diseño de interfaces", " Inteligencia artificial", " Base de datos"]
},
{
    matricula:"2020030326",
    nombre: "Pedro Antonio Sanchez Salas",
    sexo: "H",
    materias:["Inglés", " Programación", " Diseño web"]
},
{
    matricula:"2020030327",
    nombre: "Brian Eduardo Ríos Muñoz",
    sexo: "H",
    materias:["Desarrollo web", " Matemáticas", " Inglés"]
},
{
    matricula:"2020030328",
    nombre: "Carlos alberto García Medrano",
    sexo: "H",
    materias:["Geografía", " Matemáticas", " Base de datos"]
}
]



app.get('/', (req, res)=>{
    //res.send("<h1>Iniciamos con express</h1>");
    res.render('index',{titulo: "Lista de alumnos", listado:datos})
    
    
})


app.get("/tablas", (req, res)=>{
    const valores = {
        tabla:req.query.tabla
    }
    res.render('tablas', valores);

})

app.post("/tablas", (req, res)=>{
    const valores = {
        tabla:req.body.tabla
    }
    res.render("tablas", valores);
})



app.get('/cotizacion', (req, res) => {
    const resultados = {
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        plazos:req.query.plazos
       
    }
    
    res.render('cotizacion', resultados);
  });

app.post('/cotizacion', (req, res)=>{
    const resultados = {
        
        valor:req.body.valor,
        pInicial:req.body.pInicial,
        plazos:req.body.plazos
    }
    res.render('cotizacion', resultados);
})
  










/*app.post('/cotizacion', (req, res) => {
    const valor = parseFloat(req.body.valor);
    const pInicial = parseFloat(req.body.pInicial);
    const plazos = parseFloat(req.body.plazos);
  
    let pagoInicial = 0;
    let totalFin = 0;
    let pagoMensual = 0;
  
    pagoInicial = valor * (pInicial / 100);
    totalFin = valor - pagoInicial;
    pagoMensual = totalFin / plazos;
  
    res.render('cotizacion', {valor, pInicial, plazos, pagoInicial, totalFin, pagoMensual});
  });*/




app.use((req, res, next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html');
});

// Escuchar al servidor por el puerto 3000
const puerto = 3001; // Victor
app.listen(puerto, ()=>{
    console.log("iniciando puerto 3001")
})


